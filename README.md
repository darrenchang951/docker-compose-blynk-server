# docker-compose-blynk-server
A docker-compose used to deploy local blynk server.

# How to use
1. Copy the file `blynk-config/server.properties.sample` to `blynk-config/server.properties`.
```
cp blynk-config/server.properties.sample blynk-config/server.properties
```
2. Copy the file `blynk-config/mail.properties.sample` to `blynk-config/mail.properties`.
```
cp blynk-config/mail.properties.sample blynk-config/mail.properties
```
3. Run docker-compose command
```
docker-compose up -d
```

# Configure
- You can configure `blynk-config/server.properties` and `blynk-config/mail.properties`.
